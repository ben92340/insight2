# Local installation  


## 1. Pull image
```
$ docker pull elasticsearch:6.5.4 
```  

## 2. Create user defined network

Create user defined network (useful for connecting to other services attached to the same network (e.g. Kibana)):  
```
$ docker network create elk_network  
```

## 3. Prepare volume for data and log

  
The container runs Elasticsearch as user elasticsearch using uid:gid 1000:1000. Bind mounted host directories and files, such as custom_elasticsearch.yml above, need to be accessible by this user  
```
$ mkdir /ELASTIC_INSIGHT2  
$ chmod g+rwx /ELASTIC_INSIGHT2  
$ chgrp 1000 /ELASTIC_INSIGHT2 
``` 

## 4. Create elasticsearch.yml

create **/ELASTIC_INSIGHT2/custom_elasticsearch.yml** with following content :  

```
cluster.name: insight2_cluster  
node.name: insight2_node  
network.host: 0.0.0.0 
```  

## 5. Start
```
$ docker run -d --name insight2_elasticsearch --net elk_network -p 9200:9200 -p 9300:9300 -v /ELASTIC_INSIGHT2:/usr/share/elasticsearch/data -v /ELASTIC_INSIGHT2/custom_elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml -e "discovery.type=single-node" elasticsearch:6.5.4  
  ```
**Check cluster state :** http://localhost:9200/_cluster/state?pretty
  
**Log :**  
docker logs insight2_elasticsearch  
  
**Stop, start :**  
docker stop insight2_elasticsearch  
docker start insight2_elasticsearch

## 6. Import data

### From local :

Create index :
```
POST http://127.0.0.1:9200/index_name
{
Past content from /test/resources/script/index
}
```

Insert data :
```
POST http://127.0.0.1:9200/index_name/document_type
{
Past content from /test/resources/script/data
}
```

### From remote :
https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-reindex.html#reindex-from-remote

Add in your elasticsearch.yml : 

```
reindex.remote.whitelist: "pral-insels0*:*"
```

Example of request for reindex 100 documents :

```
POST _reindex
{
  "size": 100,
  "source": {
    "remote": {
      "host": "http://pral-insels01.workit.fr:9200/"
    },
    "index": "logstash-workit-fr-offers-2019-01-21",    
    "query": {
      "match_all": {}
    }
  },
  "dest": {
    "index": "logstash-workit-fr-offers-2019-01-21"
  }
}
```


## PRODUCTION 

Install java :
```
sudo apt-get install openjdk-8-jre-headless
```

You may need to install the apt-transport-https package on Debian before proceeding:
```
sudo apt-get install apt-transport-https
```

Save the repository definition to /etc/apt/sources.list.d/elastic-6.x.list:
```
echo "deb https://artifacts.elastic.co/packages/6.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-6.x.list
```

Install Elastic :
```
sudo apt-get update && sudo apt-get install elasticsearch
```

Use the update-rc.d command to configure Elasticsearch to start automatically when the system boots up:
```
sudo update-rc.d elasticsearch defaults 95 10
```

Edit file configuration :

```
vi /etc/elasticsearch/elasticsearch.yml 


cluster.name: insight2_cluster  
node.name: insight2_node  
network.host: 0.0.0.0 
discovery.zen.ping.unicast.hosts: ["10.10.0.105"]
```

Elasticsearch can be started and stopped using the service command:
```
sudo -i service elasticsearch start
sudo -i service elasticsearch stop
```

If Elasticsearch fails to start for any reason, it will print the reason for failure to STDOUT. Log files can be found in /var/log/elasticsearch/.


## Advanced configuration  :
[Readme](CONFIGURATION_ELASTIC.md)