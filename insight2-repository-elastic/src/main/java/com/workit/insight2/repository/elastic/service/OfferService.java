package com.workit.insight2.repository.elastic.service;

import com.workit.insight2.repository.elastic.entity.Offer;
import com.workit.insight2.repository.elastic.repository.OfferRepository;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.BucketOrder;
import org.elasticsearch.search.aggregations.bucket.terms.LongTerms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.cardinality.CardinalityAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.cardinality.InternalCardinality;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 *
 *
 * This class use :
 *
 * - ElasticsearchTemplate - It is a Template class which implements the ElasticsearchOperations.
 * It is more powerful than ElasticsearchRepository.
 * It has operations to create, delete indexes, bulk upload. It can do aggregated search as well.
 *
 *
 * - ElasticsearchRepository - If we define an interface which extends the ElasticsearchRepository,
 * which is provided by Spring data Elasticsearch, it will provide the CRUD operations automatically for that Document.
 *
 *
 */

@Repository
public class OfferService {

    @Autowired
    private OfferRepository repository;

    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    public List<Offer> findAll() {
        SearchQuery getAllQuery = new NativeSearchQueryBuilder()
                .withQuery(matchAllQuery()).build();
        return elasticsearchTemplate.queryForList(getAllQuery, Offer.class);
    }

    public Offer save(Offer offer) {
        return repository.save(offer);
    }

    public Long deleteByCatalogProductTitle(String productTitle) {
        return repository.deleteByCatalogProductTitle(productTitle);
    }

    public List<Offer> findAllByCatalogProductTitle(String catalogProductTitle) {
        return repository.findAllByCatalogProductTitle(catalogProductTitle);
    }

    public Page<Offer> findAllByCatalogProductTitle(String catalogProductTitle, PageRequest pageRequest) {
        return repository.findAllByCatalogProductTitle(catalogProductTitle, pageRequest);
    }


    public void get_commmon_ref_sold_by_with_avg_price(List<Long> catalogProductIds) {


        final TermsAggregationBuilder aggregation = AggregationBuilders.terms("agg")
                .field("catalogProductId")
                .size(10000)
                // get avg within a bucket
                .subAggregation(AggregationBuilders.avg("price.referenceValue"));

        SearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withIndices("logstash-workit-fr-offers-2019-01-*")
                .withQuery(boolQuery().must(termsQuery("catalogProductId", catalogProductIds)).mustNot(termQuery("siteId", "1")))
                .addAggregation(aggregation)
                .withPageable(new PageRequest(0, 1)).build();

        Aggregations aggregations = elasticsearchTemplate.query(searchQuery,
                response -> response.getAggregations());


        for (Aggregation aggs : aggregations) {
            LongTerms sum = (LongTerms) aggs;
            List<LongTerms.Bucket> sumValue = sum.getBuckets();
            for (Terms.Bucket bucket : sumValue) {
                System.out.println(bucket);
            }
        }

    }

    private Long count_commmon_ref_sold_by(List<Long> catalogProductIds) {

        Long result = 0L;

        CardinalityAggregationBuilder aggregation = AggregationBuilders.cardinality("top_catalogProductId")
                .field("catalogProductId");


        SearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withIndices("logstash-workit-fr-offers-2019-01-*")
                .withQuery(boolQuery().must(termsQuery("catalogProductId", catalogProductIds)).mustNot(termQuery("siteId", "1")))
                .addAggregation(aggregation)
                .withPageable(new PageRequest(0, 1)).build();

        Aggregations aggregations = elasticsearchTemplate.query(searchQuery,
                response -> response.getAggregations());

        for (Aggregation aggs : aggregations) {
            InternalCardinality sum = (InternalCardinality) aggs;
            result += sum.getValue();
        }

        System.out.println(result);

        return result;
    }


    private List<Long> get_last_product_create() {


        final TermsAggregationBuilder aggregation = AggregationBuilders.terms("agg")
                .field("catalogProductId")
                .size(10000)
                // sort within a bucket
                .subAggregation(AggregationBuilders.topHits("lastDate").sort("productCreationDateTime"))
                // sort between buckets
                .subAggregation(AggregationBuilders.max("top_hit").field("productCreationDateTime"))
                .order(BucketOrder.aggregation("top_hit", false));

/*		TermsAggregationBuilder aggregation = AggregationBuilders.terms("agg")
				.field("catalogProductId")
				.size(10000)
				// Sort buckets
				.order(BucketOrder.aggregation("min", false))
				.subAggregation(AggregationBuilders.max("min")
						.field("productCreationDateTime"))
				// Sort (ASC) each bucket documents
				// and pick first bucket document
				.subAggregation(AggregationBuilders.topHits("top")
						.sort("productCreationDateTime")
						.size(1));*/

        SearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withIndices("logstash-workit-fr-offers")
                .withQuery(boolQuery().must(termQuery("catalogId", "1250")).must(termQuery("siteId", "1")))
                .addAggregation(aggregation)
                .withPageable(new PageRequest(0, 1)).build();


        Aggregations aggregations = elasticsearchTemplate.query(searchQuery,
                response -> response.getAggregations());


        List<Long> listReferenceSellBySite1 = new ArrayList<>();


        for (Aggregation aggs : aggregations) {
            LongTerms sum = (LongTerms) aggs;
            List<LongTerms.Bucket> sumValue = sum.getBuckets();
            for (Terms.Bucket bucket : sumValue) {
                System.out.println(bucket);
            }
        }

        return listReferenceSellBySite1;
    }

    private List<Long> get_ref_sold_by() {

        //Get distinct liste of ref sold by site 1
        TermsAggregationBuilder aggregation = AggregationBuilders.terms("top_catalogProductId")
                .field("catalogProductId")
                .size(10000);

        SearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withIndices("logstash-workit-fr-*")
                .withQuery(boolQuery().must(termQuery("catalogId", "1250")).must(termQuery("siteId", "1")))
                .addAggregation(aggregation)
                .withPageable(new PageRequest(0, 1)).build();


        Aggregations aggregations = elasticsearchTemplate.query(searchQuery,
                response -> response.getAggregations());


        List<Long> listReferenceSellBySite1 = new ArrayList<>();


        for (Aggregation aggs : aggregations) {
            LongTerms sum = (LongTerms) aggs;
            List<LongTerms.Bucket> sumValue = sum.getBuckets();
            for (Terms.Bucket bucket : sumValue) {
                listReferenceSellBySite1.add((Long) bucket.getKey());
            }
        }

        return listReferenceSellBySite1;
    }


    private void cardinality_test_common_ref_sold_by() {

        //Get distinct liste of ref sold by site 1
        CardinalityAggregationBuilder aggregation = AggregationBuilders.cardinality("top_catalogProductId")
                .field("catalogProductId");


        SearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withIndices("logstash-workit-fr-offers")
                .withQuery(boolQuery().must(termQuery("catalogId", "1250")).must(termQuery("siteId", "1")))
                //TODO genere un post_filter.. PAS BON !!
                //.withFilter(boolQuery().must(termQuery("catalogId", "1250")).must(termQuery("siteId", "1")))
                .addAggregation(aggregation)
                .withPageable(new PageRequest(0, 1)).build();

        Aggregations aggregations = elasticsearchTemplate.query(searchQuery,
                response -> response.getAggregations());


        for (Aggregation aggs : aggregations) {
            InternalCardinality sum = (InternalCardinality) aggs;
            System.out.println(sum.getValue());
        }
    }


    private void test_top_sell_catalogProductId() {

        TermsAggregationBuilder aggregation = AggregationBuilders.terms("top_catalogProductId")
                .field("catalogProductId");


        SearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(QueryBuilders.matchQuery("siteId", "1"))
                .addAggregation(aggregation)
                .withPageable(new PageRequest(0, 1)).build();


        Aggregations aggregations = elasticsearchTemplate.query(searchQuery,
                response -> response.getAggregations());


        for (Aggregation aggs : aggregations) {
            LongTerms sum = (LongTerms) aggs;
            List<LongTerms.Bucket> sumValue = sum.getBuckets();
            for (Terms.Bucket bucket : sumValue) {
                System.out.println("Key " + bucket.getKey() + " sumValue= " + bucket.getDocCount());
            }
        }

        //List<Offer> articles = elasticsearchTemplate.queryForList(searchQuery, Offer.class);

        System.out.println(aggregations.getAsMap());
    }


}
