/*
 * Copyright 2012-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.workit.insight2.repository.elastic;

import com.workit.insight2.repository.elastic.service.OfferService;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;

import java.net.InetAddress;
import java.net.UnknownHostException;


/**
 *
 * @EnableAutoConfiguration for autoconfiguration of ElasticsearchRepository
 * @Import(OfferService.class) (can be replace with ComponentScan)
 *
 */
@EnableAutoConfiguration
@Import(OfferService.class)
@PropertySource(value = "classpath:application-elastic-${spring.profiles.active:local}.properties")
public class RepositoryElasticConfig {

	@Value(value = "${elasticsearch.cluster.node1.port}")
	private String port;

	@Value(value = "${elasticsearch.cluster.node1.url}")
	private String url;

	@Value(value = "${elasticsearch.cluster.name}")
	private String clusterName;

	@Bean
	private Client client() throws UnknownHostException {
		Settings elasticsearchSettings = Settings.builder()
				//.put("client.transport.sniff", true)
				.put("cluster.name", clusterName).build();

		TransportClient client = new PreBuiltTransportClient(elasticsearchSettings);
		client.addTransportAddress(new TransportAddress(InetAddress.getByName(url), Integer.parseInt(port)));
		return client;
	}

	@Bean
	private ElasticsearchTemplate elasticsearchTemplate() throws UnknownHostException {
		return new ElasticsearchTemplate(client());
	}

}
