/*
 * Copyright 2012-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.workit.insight2.repository.elastic.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.Date;
import java.util.Objects;

@Document(indexName = "insight2-offer", type = "offer")
public class Offer {

	@Id
	private Long id;

	private String catalogProductId;

	private String catalogProductTitle;

	private Date dateCreated;

	public String getCatalogProductId() {
		return catalogProductId;
	}

	public void setCatalogProductId(String catalogProductId) {
		this.catalogProductId = catalogProductId;
	}

	public String getCatalogProductTitle() {
		return catalogProductTitle;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setCatalogProductTitle(String catalogProductTitle) {
		this.catalogProductTitle = catalogProductTitle;
	}


	public Offer() {
	}

	public Offer(String catalogProductId, String catalogProductTitle) {
		this.catalogProductId = catalogProductId;
		this.catalogProductTitle = catalogProductTitle;
	}

	public Offer(String catalogProductId, String catalogProductTitle, Date dateCreated) {
		this.catalogProductId = catalogProductId;
		this.catalogProductTitle = catalogProductTitle;
		this.dateCreated = dateCreated;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Offer offer = (Offer) o;
		return Objects.equals(id, offer.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
}
