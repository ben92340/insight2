/*
 * Copyright 2012-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.workit.insight2.repository.elastic.repository;

import com.workit.insight2.repository.elastic.entity.Offer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

public interface OfferRepository extends ElasticsearchRepository<Offer, String> {

    List<Offer> findAllByCatalogProductTitle(String title);

    Page<Offer> findAllByCatalogProductTitle(String author, Pageable pageable);

    Long deleteByCatalogProductTitle(String lastname);

}
