package com.workit.insight2.repository.elastic.TI;

import com.workit.insight2.repository.elastic.RepositoryElasticConfig;
import com.workit.insight2.repository.elastic.entity.Offer;
import com.workit.insight2.repository.elastic.service.OfferService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 *
 * Integration test with real spring context + Use application-elastic-local.properties
 * Create index if not exist
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RepositoryElasticConfig.class)
public class OfferServiceLocalTestIt {

    @Autowired
    private OfferService service;

	@Autowired
	ElasticsearchTemplate esTemplate;


    @Test
    public void testSave() {

        Offer offer = new Offer("1001", "Elasticsearch", new Date());
        Offer testOffer = service.save(offer);

        assertEquals(testOffer.getCatalogProductId(), offer.getCatalogProductId());
        assertEquals(testOffer.getCatalogProductTitle(), offer.getCatalogProductTitle());
    }

    @Test
    public void testFindByTitle() {

        Offer offer = new Offer("1001", "Elasticsearch");
        service.save(offer);

        List<Offer> byTitle = service.findAllByCatalogProductTitle(offer.getCatalogProductTitle());
        assertTrue(byTitle.size() > 0);
    }


    @Test
    public void testFindByTitlePageable() {
        Offer offer = new Offer("1001", "Elasticsearch");

        service.save(offer);

        Page<Offer> byTitle = service.findAllByCatalogProductTitle(offer.getCatalogProductTitle(), new PageRequest(0, 10));
        assertTrue(byTitle.getTotalElements() > 0);
    }

}

