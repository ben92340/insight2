package com.workit.insight2.repository.elastic.TI;

import com.workit.insight2.repository.elastic.RepositoryElasticConfig;
import com.workit.insight2.repository.elastic.entity.Offer;
import com.workit.insight2.repository.elastic.service.OfferService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;


/**
 * Integration test with real spring context + Use application-elastic-prod.properties
 *
 * Test only basic CRUD operation in order to check connexion and user right to production cluster
 *
 * @SpringBootTest tells Spring Boot to go and look for a main configuration class (one with @SpringBootApplication for instance), and use that to start a Spring application context
 * @ActiveProfiles("prod") to load application-elastic-prod.properties
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RepositoryElasticConfig.class)
@ActiveProfiles("prod")
public class OfferServiceProdTestIt {


    @Autowired
    private OfferService service;

	@Autowired
	ElasticsearchTemplate esTemplate;

	private Offer offer = new Offer(UUID.randomUUID().toString(), UUID.randomUUID().toString());

    @Test
	public void testCRUD() {
        //CREATE
        Offer testOffer = service.save(offer);
        assertEquals(testOffer.getCatalogProductId(), offer.getCatalogProductId());
        assertEquals(testOffer.getCatalogProductTitle(), offer.getCatalogProductTitle());

        //READ
        List<Offer> byTitle = service.findAllByCatalogProductTitle(offer.getCatalogProductTitle());
        assertEquals(1, byTitle.size());

        //DELETE
        long nbDelete = service.deleteByCatalogProductTitle(offer.getCatalogProductTitle());
        assertEquals(1, nbDelete);
    }

}

