package com.workit.insight2.repository.elastic.TU;

import com.workit.insight2.repository.elastic.entity.Offer;
import com.workit.insight2.repository.elastic.repository.OfferRepository;
import com.workit.insight2.repository.elastic.service.OfferService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;


/**
 *
 * Unit test with custom spring context
 *
 */
@RunWith(SpringRunner.class)
@ContextConfiguration( classes = OfferServiceTest.TestConfig.class)
public class OfferServiceTest {

	@Autowired
	private OfferService offerService;

	@MockBean
	private ElasticsearchTemplate elasticsearchTemplate;


    @Test
    public void test_find_all() {
    	//GIVEN
		Offer offer = new Offer("alex", "toto");
		List<Offer> allOffers = Arrays.asList(offer);
		Mockito.when(elasticsearchTemplate.queryForList(ArgumentMatchers.any(SearchQuery.class), ArgumentMatchers.eq(Offer.class)))
		.thenReturn(allOffers);

		//WHEN
		List<Offer> result = offerService.findAll();

		//THEN
		Assert.assertEquals(allOffers, result);
    }


	@Configuration
	static class TestConfig{

    	@Bean
		public OfferService offerService(){
    		return new OfferService();
		}

		@Bean
		public OfferRepository offerRepository(){
			return Mockito.mock(OfferRepository.class);
		}

	}


}

