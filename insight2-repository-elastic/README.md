# Repository for elastic cluster of insight2

## Import as maven dependency :
```
<dependency>
        <groupId>com.workit.insight2</groupId>
        <artifactId>insight2-repository-elastic</artifactId>
        <version>0.0.1-SNAPSHOT</version>
</dependency>
```
## Main spring config file :
```
com.workit.insight2.admin.server.RepositoryElasticConfig.java
```

## Installation of elasticsearch :
[README](INSTALL_ELASTIC.md)

## Configuration of elasticsearch :
[README](CONFIGURATION_ELASTIC.md)
