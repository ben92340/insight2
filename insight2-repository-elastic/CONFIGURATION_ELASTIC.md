# Elasticsearch configuration 

## Hardware configuration
TODO
https://www.elastic.co/guide/en/elasticsearch/guide/current/hardware.html

## Cluster configuration
```
sudo vi /etc/elasticsearch/elasticsearch.yml
```
Don’t reuse the same cluster names in different environments, otherwise you might end up with nodes joining the wrong cluster.
```
cluster.name: insight2_cluster_{ENV_NAME}
```
By default, Elasticsearch binds to loopback addresses only — e.g. 127.0.0.1 and [::1]. This is sufficient to run a single development node on a server.
The node will bind to this hostname or IP address and publish (advertise) this host to other nodes in the cluster. 
Note the addition of "_local_", which configures Elasticsearch to also listen on all loopback devices. This will allow you to use the Elasticsearch HTTP API locally
```
network.host:  [_tun0_, _local_]
```
Locations of the data and log folder:
```
path.logs: /var/log/elasticsearch
path.data: /var/data/elasticsearch
```

## Node configuration
https://www.elastic.co/guide/en/elasticsearch/reference/current/modules-node.html

```
node.name: ${HOSTNAME}
```

- Architecture V1 :

By default a node is a master-eligible node and a data node, plus it can pre-process documents through ingest pipelines. This is very convenient for small clusters but, as the cluster grows, it becomes important to consider separating dedicated master-eligible nodes from dedicated data nodes.
3 ou 2 default node :
```
node.master (enabled by default)
node.data (enabled by default)
node.ingest (enabled by default)
cluster.remote.connect (enabled by default)
```
- Architecture V2 : 
Cf. TODO

## Prevent data loss
To prevent data loss, it is vital to configure the discovery.zen.minimum_master_nodes setting (which defaults to 1).
```
discovery.zen.minimum_master_nodes = (master_eligible_nodes / 2) + 1
```

## Discovery 
https://www.elastic.co/guide/en/elasticsearch/reference/current/modules-discovery-zen.html
```
discovery.zen.ping.unicast.hosts: ["10.0.0.1", "0.0.0.0", "seeds.mydomain.com"]
```
"seeds.mydomain.com" is a hostname that resolves to multiple IP addresses will try all resolved addresses.

## Disable swapping / Enable Memory Locking
https://www.elastic.co/guide/en/elasticsearch/reference/current/setup-configuration-memory.html

Elastic recommends to avoid swapping the Elasticsearch process at all cost

To disable it permanently, you will need to :
 ```
edit the /etc/fstab file and comment out any lines that contain the word swap.
```
Another option is to use mlockall on Linux/Unix systems :
```
bootstrap.memory_lock: true
```
**Check value :**
GET _nodes?filter_path=**.mlockall

If you see that mlockall is false, then it means that the mlockall request has failed, you have to try other solution.

## Heap size 
https://www.elastic.co/guide/en/elasticsearch/reference/6.5/heap-size.html
```
Edit config/jvm.options (when installing from the tar or zip distributions) or /etc/elasticsearch/jvm.options.
Add -Xmx32g -Xms32g
```
Set the minimum heap size (Xms) and maximum heap size (Xmx) to be equal to each other.
set it to about 50% of your available memory.

## FIle Descriptor 
https://www.elastic.co/guide/en/elasticsearch/reference/current/file-descriptors.html

Running out of file descriptors can be disastrous and will most probably lead to data loss.
```
For the .zip and .tar.gz packages, set ulimit -n 65536 as root before starting Elasticsearch
or set nofile to 65536 in /etc/security/limits.conf :
vi /etc/security/limits.conf 
Add line : elasticsearch  -  nofile  65536
```
**Check value :** 
GET _nodes/stats/process?filter_path=**.max_file_descriptors 

## Virtual memory 
https://www.elastic.co/guide/en/elasticsearch/reference/current/vm-max-map-count.html

The default operating system limits on mmap counts is likely to be too low, which may result in out of memory exceptions.
```
To set this value permanently, update the vm.max_map_count setting in /etc/sysctl.conf
```
**To verify :** after rebooting, run sysctl vm.max_map_count
## Number of thread
https://www.elastic.co/guide/en/elasticsearch/reference/current/max-number-of-threads.html

It is important that it is able to create new threads whenever needed. 
```
This can be done by setting ulimit -u 4096 as root before starting Elasticsearch, 
or by setting nproc to 4096 in /etc/security/limits.conf.
```

## Shard and replica 
We use default setting : each index are created with 5 shards and 1 replica

## Index by date 
https://www.elastic.co/guide/en/elasticsearch/reference/current/date-index-name-processor.html

- Create pipeline
```
PUT _ingest/pipeline/monthlyindex
{
  "description": "monthly date-time index naming",
  "processors" : [
    {
      "date_index_name" : {
        "field" : "date1",
        "index_name_prefix" : "insight2-",
        "date_rounding" : "m"
      }
    }
  ]
}
```

**date_rounding** : Valid values are: y (year), M (month), w (week), d (day), h (hour), m (minute) and s (second). 

- Use pipeline during reindex :

```
POST _reindex
{
  "source": {
    "index": "insight2"
  },
  "dest": {
    "index": "insight2-",
    "pipeline": "monthlyindex"
  }
}
```

- See curent pipeline : GET /_ingest/pipeline


## Delete index 
https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-delete-index.html

DELETE /index_name

## Alias
https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-aliases.html

The index aliases API allows aliasing an index with a name, with all APIs automatically converting the alias name to the actual index name.
```
PUT /_aliases
{
    "actions" : [
        { "add" : { "index" : "test*", "alias" : "all_test_indices" } }
    ]
}
```

- ! It will not automatically update as new indices that match this pattern are added/removed.
- ! It is an error to index to an alias which points to more than one index.

Or we can create during index creation : https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-create-index.html#create-index-aliases

- The rest endpoint is: /{index}/_alias/{alias}.
- Aliases info : GET /_cat/aliases?v ou GET /_aliases

## Log
https://www.elastic.co/guide/en/elasticsearch/reference/current/logging.html

If your log directory (path.logs) is /var/log/elasticsearch and your cluster is named production then ${sys:es.logs.base_path} will resolve to /var/log/elasticsearch and ${sys:es.logs.base_path}${sys:file.separator}${sys:es.logs.cluster_name}.log will resolve to /var/log/elasticsearch/production.log.

# TODO

### Automatique delete indices
Example of automatically delete indices older than 45 days (based on index name)
https://www.elastic.co/guide/en/elasticsearch/client/curator/current/ex_delete_indices.html

### Architecture cible
Before configuring dedicated master nodes, ensure that your cluster will have at least 3 master-eligible nodes.

1 dedicated master-eligible node : it is important for the stability of the cluster that master-eligible nodes do as little work as possible.
```
node.master: true 
node.data: false 
node.ingest: false 
cluster.remote.connect: false 
```

2 data node : handle data related operations like CRUD, search, and aggregations. These operations are I/O-, memory-, and CPU-intensive. It is important to monitor these resources and to add more data nodes if they are overloaded.
```node.master: false 
node.data: true 
node.ingest: false 
cluster.remote.connect: false 
```

1 ingest node : can execute pre-processing pipelines, composed of one or more ingest processors. Use during _reindex
```node.master: false 
node.data: false 
node.ingest: true 
cluster.remote.connect: false
```

? dedicated coordinating node : Coordinating only nodes can benefit large clusters by offloading the coordinating node role from data and master-eligible nodes.
```node.master: false 
node.data: false 
node.ingest: false 
cluster.remote.connect: false```
