# REST API for insight 2

## Run :
- From your IDE as a simple Java application :
   Main : com.workit.insight2.rest.api.RestApiApplication.java
- mvn spring-boot:run
- java -jar target/insight2-rest-api-0.0.1-SNAPSHOT.jar

## Swagger UI :
http://localhost:8080/insight2-rest-api/swagger-ui.html
#### With Springfox at :
http://localhost:8080/insight2-rest-api/v2/api-docs
