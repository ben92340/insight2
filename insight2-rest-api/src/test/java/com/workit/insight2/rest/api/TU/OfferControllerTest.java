package com.workit.insight2.rest.api.TU;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.workit.insight2.repository.elastic.entity.Offer;
import com.workit.insight2.repository.elastic.service.OfferService;
import com.workit.insight2.rest.api.controller.OfferController;
import com.workit.insight2.rest.api.request.OfferRequest;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


/**
 *
 * Unit test with custom spring context
 *
 * @WebMvcTest Using this annotation will disable full auto-configuration and instead apply only configuration relevant to MVC tests
 *
 */
@RunWith(SpringRunner.class)
@WebMvcTest(OfferController.class)
public class OfferControllerTest {

	@Autowired
	private MockMvc mvc;

	@MockBean
	private OfferService offerService;

	@Test
	public void test_find_all() throws Exception {
		//GIVEN
		List<Offer> allOffers = Arrays.asList(new Offer("alex", "toto"));
		BDDMockito.given(offerService.findAll()).willReturn(allOffers);

		//WHEN
		mvc.perform(MockMvcRequestBuilders.post("/api/findAll")
				.content(new ObjectMapper().writeValueAsString(new OfferRequest()))
				.contentType(MediaType.APPLICATION_JSON))
		//THEN
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", Matchers.hasSize(1)))
				.andExpect(jsonPath("$[0].catalogProductTitle", CoreMatchers.is("toto")));
	}

}

