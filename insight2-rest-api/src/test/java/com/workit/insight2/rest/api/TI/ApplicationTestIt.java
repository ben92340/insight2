package com.workit.insight2.rest.api.TI;

import com.workit.insight2.rest.api.RestApiApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Integration test with real spring context + Use application-elastic-default.properties
 *
 * Test only loading context
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RestApiApplication.class)
public class ApplicationTestIt {

	@Test
	public void contextLoads() {
	}

}

