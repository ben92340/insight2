package com.workit.insight2.rest.api;

import com.workit.insight2.kafka.consumer.KafkaConsumerApplication;
import com.workit.insight2.repository.elastic.RepositoryElasticConfig;
import com.workit.insight2.rest.api.config.SecurityPermitAllConfig;
import com.workit.insight2.rest.api.config.SwaggerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import({RepositoryElasticConfig.class, SwaggerConfig.class, SecurityPermitAllConfig.class, KafkaConsumerApplication.class})
public class RestApiApplication {

	private static final Logger logger = LoggerFactory.getLogger(RestApiApplication.class);

	public static void main(String[] args) {
		logger.info("Starting RestApiApplication");
		SpringApplication.run(new Class[]{RestApiApplication.class }, args);
	}

}

