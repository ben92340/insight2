package com.workit.insight2.rest.api.request;

import io.swagger.annotations.ApiModelProperty;

public class OfferRequest {

    @ApiModelProperty(value = "Custom swagger documentation")
    private long someProperty;

    public long getSomeProperty() {
        return someProperty;
    }

    public void setSomeProperty(long someProperty) {
        this.someProperty = someProperty;
    }
}
