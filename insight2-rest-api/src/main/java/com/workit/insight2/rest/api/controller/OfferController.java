package com.workit.insight2.rest.api.controller;

import com.workit.insight2.repository.elastic.entity.Offer;
import com.workit.insight2.repository.elastic.service.OfferService;
import com.workit.insight2.rest.api.request.OfferRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api( description="API for offers")
@RestController()
@RequestMapping("/api")
public class OfferController {

    @Autowired
    OfferService offerService;

    @ApiOperation(value = "Custom swagger info")
    @PostMapping(value = "/findAll", consumes = MediaType.APPLICATION_JSON_VALUE)

    public Iterable<Offer> findAll(@RequestBody OfferRequest offerRequest) {
        return offerService.findAll();
    }


}