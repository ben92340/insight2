package com.workit.insight2.kafka.consumer;

import com.workit.insight2.kafka.consumer.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@EmbeddedKafka(brokerProperties = {"listeners=PLAINTEXT://${spring.embedded.kafka.brokers}"})
public class SpringBootKafkaProducerExampleApplicationTests {

    private static final String TOPIC = "test";

    @Autowired
    private KafkaTemplate<String, User> kafkaTemplate;

    @Test
    public void tearDown() {
        kafkaTemplate.send(TOPIC, new User());
    }
}
