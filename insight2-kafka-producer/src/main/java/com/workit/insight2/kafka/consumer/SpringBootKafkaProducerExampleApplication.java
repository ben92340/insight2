package com.workit.insight2.kafka.consumer;

import com.workit.insight2.kafka.consumer.config.KakfaConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(KakfaConfiguration.class)
public class SpringBootKafkaProducerExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootKafkaProducerExampleApplication.class, args);
    }

}
