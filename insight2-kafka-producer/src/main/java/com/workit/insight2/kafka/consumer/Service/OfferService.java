package com.workit.insight2.kafka.consumer.Service;

import com.workit.insight2.kafka.consumer.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class OfferService {

    @Value(value = "${topic.name.offer}")
    private String topicName;

    @Autowired
    private KafkaTemplate<String, User> kafkaTemplate;

    public void sendMessage(User msg) {
        kafkaTemplate.send(topicName, msg);
    }

}
