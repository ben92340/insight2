# Spring Boot Admin 
https://codecentric.github.io/spring-boot-admin/current/

Manage and monitor Spring Boot applications. 
The applications register with Spring Boot Admin Client (via HTTP). 
The UI is just a Vue.js application on top of the Spring Boot Actuator endpoints.

## Run :
- From your IDE as a simple Java application :
   Main : com.workit.insight2.admin.server.AdminServerApplication.java
- mvn spring-boot:run
- java -jar target/insight2-admin-server-0.0.1-SNAPSHOT.jar

## Admin UI :
http://localhost:8091/insight2-admin-server

## Client configuration :
https://codecentric.github.io/spring-boot-admin/current/#register-clients-via-spring-boot-admin with 
```
spring.boot.admin.client.url=http://localhost:8091/insight2-admin-server
```