# Spring Boot with Kafka Producer Example



## Import as maven dependency :
```
<dependency>
        <groupId>com.workit.insight2</groupId>
        <artifactId>insight2-repository-elastic</artifactId>
        <version>0.0.1-SNAPSHOT</version>
</dependency>
```
## Main spring config file :
```
com.workit.insight2.admin.server.RepositoryElasticConfig.java
```


cd {kafka_dir_installation}/kafka_2.11-2.1.0

#### Start Zookeeper

* `bin/zookeeper-server-start.sh config/zookeeper.properties`

#### Start Kafka Server

* `bin/kafka-server-start.sh config/server.properties`

#### Create Kafka Topic

* `bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic Kafka_Example`

#### Consume from the Kafka Topic via Console

`bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic Kafka_Example --from-beginning`

#### Publish message via WebService

* `http://localhost:8080/kafka/publish/naruto`

* `http://localhost:8080/kafka/publish/sasuke`
