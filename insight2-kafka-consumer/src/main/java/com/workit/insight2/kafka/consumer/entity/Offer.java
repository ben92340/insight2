package com.workit.insight2.kafka.consumer.entity;

public class Offer {

    private String title;


    public Offer() {

    }

    public Offer(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Offer{" +
                "title='" + title + '\'' +
                '}';
    }
}



