package com.workit.insight2.kafka.consumer.config;

import com.workit.insight2.kafka.consumer.entity.Offer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ConsumerAwareListenerErrorHandler;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.messaging.MessageHeaders;

import java.util.HashMap;
import java.util.Map;


@EnableKafka
@Configuration
public class KakfaConfigurationConsumer {


    @Value(value = "${topic.name.offer}")
    private String topicName;

    @Value(value = "${kafka.host}")
    private String kafkaHost;



    @KafkaListener(groupId = "consumer2", topics = "${topic.name.offer}", errorHandler = "listen3ErrorHandler", containerFactory = "kafkaListenerContainerFactory" )
    public void listen(Offer message, Acknowledgment acknowledgment) {
        System.out.println("Received Messasge in group consumer2: " + message);
        acknowledgment.acknowledge();
    }


    @KafkaListener(groupId = "consumer1", topics = "${topic.name.offer}", errorHandler = "listen3ErrorHandler", containerFactory = "kafkaListenerContainerFactory" )
    public void listen(Acknowledgment acknowledgment) {
        acknowledgment.acknowledge();
    }

    @Bean
    public ConsumerAwareListenerErrorHandler listen3ErrorHandler() {
        return (m, e, c) -> {
            MessageHeaders headers = m.getHeaders();
            c.seek(new org.apache.kafka.common.TopicPartition(
                            headers.get(KafkaHeaders.RECEIVED_TOPIC, String.class),
                            headers.get(KafkaHeaders.RECEIVED_PARTITION_ID, Integer.class)),
                    headers.get(KafkaHeaders.OFFSET, Long.class));
            return null;
        };
    }


    private  ConsumerFactory<String, String> consumerFactory() {
        Map<String, Object> props = new HashMap<>();
        props.put(
                ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,
                kafkaHost);

        /*If all the consumer instances have the same consumer group, then the records will effectively be load balanced over the consumer instances
        If all the consumer instances have different consumer groups, then each record will be broadcast to all the consumer processes.*/

        props.put(
                ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
                StringDeserializer.class);
        props.put(
                ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
                JsonDeserializer.class);


        props.put(JsonDeserializer.TRUSTED_PACKAGES, "*");
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, false);
        //See https://kafka.apache.org/documentation/#consumerconfigs



        //TODO check if needed
        //props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

        return new DefaultKafkaConsumerFactory<>(props);
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, String>
    kafkaListenerContainerFactory() {

        ConcurrentKafkaListenerContainerFactory<String, String> factory
                = new ConcurrentKafkaListenerContainerFactory<>();

        factory.setConsumerFactory(consumerFactory());

        factory.getContainerProperties().setAckOnError(true);
        //Necessaire si ENABLE_AUTO_COMMIT_CONFIG à false
        factory.getContainerProperties().setAckMode(ContainerProperties.AckMode.MANUAL_IMMEDIATE);

        //factory.setErrorHandler();

        return factory;
    }

}