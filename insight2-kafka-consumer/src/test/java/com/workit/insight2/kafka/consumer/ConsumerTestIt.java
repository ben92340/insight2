package com.workit.insight2.kafka.consumer;

import com.workit.insight2.kafka.consumer.entity.Offer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringSerializer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/***
 *
 * Test real Spring Application context with Kafka host load from application.properties
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ConsumerTestIt {

    @Value(value = "${topic.name.offer}")
    private String topicName;

    @Value(value = "${kafka.host}")
    private String kafkaHost;

    @Before
    public void sendData() throws ExecutionException, InterruptedException {
        Map<String, Object> configs = new HashMap<>(KafkaTestUtils.senderProps(kafkaHost));

        Producer<String, Object> producer = new DefaultKafkaProducerFactory<>(configs, new StringSerializer(), new JsonSerializer<>()).createProducer();

        Future<RecordMetadata> m =  producer.send(new ProducerRecord<>(topicName, new Offer("Test-ben4")));
        System.out.println("Message produced, topic: " + m.get().topic());
        System.out.println("Message produced, partition: " + m.get().partition());
        System.out.println("Message produced, offset: " + m.get().offset());
        producer.flush();


    }

    @Test
    public void test() {
        //Verifier dans les logs que le message est bien intercepté par les @KafkaListener
    }
}
